PACKAGE_NAME = tags_indexer

all:
	docker build -t $(PACKAGE_NAME) .
	docker tag $(PACKAGE_NAME):latest nepms/$(PACKAGE_NAME):latest

push:
	make all
	docker push nepms/$(PACKAGE_NAME):latest

test:
	docker run -e TEST=true $(PACKAGE_NAME)

git:
	git commit -a
	git push