<?php
    /**
     * Created by PhpStorm.
     * User: finna
     * Date: 20/03/2018
     * Time: 16:38 PM
     */
    include(__DIR__."/include/bootstrap.php");
    include(__DIR__."/include/indexer/CElasticClient.php");
    define("DEBUG_TO_SCREEN", true);

    DEBUGOUT("Indexer ready. Looking for new/updated events...");
    $strTest = getenv("TEST");
    if($strTest == "true")
    {
        CElasticIndexScheduler::CountNewEvents();
        exit;
    }

    while(true)
    {
        CElasticIndexScheduler::IndexNewEvents();
        CElasticIndexScheduler::RemoveIndexForDeletedEvents();
        sleep(2);
    }

    class CElasticIndexScheduler
    {

        static public function RemoveIndexForDeletedEvents()
        {
            $arrDone = array();
            $db = CAPI::GetDBObject();
            $strQuery = "SELECT eventid,eventdata->>'event_id' AS event_id FROM event WHERE eventactive=0 AND event.eventindexstatus=1";

            //DEBUGOUT("Deleting indexes for events that has been deleted");
            if($db->exec($strQuery) && $db->recordCount() > 0)
            {
                $db->moveStart();
                while ($db->nextRow())
                {
                    if(CElasticClient::DeleteEvent($db->fObject()->event_id))
                    {
                        $arrDone[] = $db->fObject()->eventid;
                    }
                }

                self::SetIndexStatusNotIndexed($arrDone);
            }
        }

        static public function CountNewEvents()
        {
            $db = CAPI::GetDBObject();
            $strQuery = "SELECT count(*) FROM search_index_queue WHERE text IS NOT null ";
            if($db->exec($strQuery) && $db->fObject()>0)
            {
                DEBUGOUT(sprintf("%s new events in index queue", $db->fObject()->count));
            }
            else
            {
                DEBUGOUT("No new events in index queue");
            }
        }

        static public function IndexNewEvents()
        {
            $nTotalIndexed = 0;

            $db = CAPI::GetDBObject(true);
            while(true)
            {
                $arrDone = array();
                $strQuery = "SELECT * FROM search_index_queue WHERE text IS NOT null ORDER BY eventid ASC LIMIT 100";
//                $strQuery = "SELECT * FROM search_index_queue ORDER BY eventid ASC LIMIT 100";

                if($db->exec($strQuery) && $db->recordCount() > 0)
                {
                    $nNew = $db->recordCount();
                    DEBUGOUT("Indexing {$nNew} events...");

                    $db->moveStart();
                    $n=1;
                    while($db->nextRow())
                    {
                        $n++;
                        $objDomain = CTagsDomain::GetDomain($db->fObject()->domainid);
                        $objHappening = CTagsHappening::GetHappening($db->fObject()->happeningid);
                        $objEvent = CTagsEvent::GetEvent($db->fObject()->event_id);
                        if(CElasticClient::IndexEvent($objDomain, $objHappening, $objEvent))
                        {
                            $arrDone[] = $db->fObject()->eventid;
                        }
                        else
                        {
//                            DEBUGOUT("Error indexing event id ");
                        }
                    }
                    DEBUGOUT("Updating status...");
                    self::SetIndexStatusDone($arrDone);
                    $nTotalIndexed += count($arrDone);
                    DEBUGOUT("Indexed a total of {$nTotalIndexed} events");
                }
                else
                {
                    //                    DEBUGOUT("No more entries in index queue");
                    break;
                    //$bHasEntriesInQueue = false;
                }
            }

            //DEBUGOUT("Search index updated");
        }

        private static function SetIndexStatus($nStatus, $arrEventIds)
        {
            if($arrEventIds && is_array($arrEventIds) && count($arrEventIds)>0)
            {
                $csvEvents = implode(",", $arrEventIds);
                $strUpdate = "UPDATE event SET eventindexstatus={$nStatus} WHERE eventid IN ({$csvEvents})";
                if(CAPI::GetDBObject()->exec($strUpdate))
                {
                    DEBUGOUT("Event index status updated");
                }
                else
                {
                    DEBUGOUT("Error: Event index status NOT updated");
                }
            }
        }

        private static function SetIndexStatusDone($arrEventIds)
        {
            self::SetIndexStatus(1, $arrEventIds);
        }

        private static function SetIndexStatusNotIndexed($arrEventIds)
        {
            self::SetIndexStatus(0, $arrEventIds);
        }

        public static function DeleteAllIndexedEvents()
        {
            $strQuery = "select eventdata->>'event_id' as event_id, eventhappening_id from event";
            $db = CAPI::GetDBObject(true);
            if($db->exec($strQuery) && $db->recordCount() > 0)
            {
                $db->moveStart();
                while ($db->nextRow())
                {
                    $strId = $db->fObject()->event_id;
                    if(!CElasticClient::DeleteEvent($strId))
                    {
                        DEBUGOUT("Error deleting {$strId}");
                    }
                }
            }
        }

        public static function IsAlreadyRunning()
        {
            $arrPid = null;
            exec("ps aux | grep -i 'indexer.php' | grep -v grep", $arrPid);
            if(empty($arrPid) || count($arrPid) == 1)
            {
                return false;
            }

            return true;
        }
    }