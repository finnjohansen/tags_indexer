#name: tags_indexer
FROM nepms/tags2_main:latest
MAINTAINER Finn J. <fjohansen@nepgroup.com>

COPY indexer.php ./indexer.php
CMD php indexer.php

# docker build -t push_process .
# docker run push_process
# docker login -u finna
# docker tag push_process:latest nepms/tags1_push_process:latest
# docker push nepms/push_process:latest
